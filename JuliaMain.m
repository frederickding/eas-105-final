%% Julia Set Visualizer
% Frederick Ding
% Version (Git hash): $Id$

function JuliaMain
	% prepare a figure window so we have a height/width
	handles(1) = figure;
	position = get(handles(1), 'Position');
	set(handles(1), 'MenuBar', 'none', 'Toolbar', 'figure', ... 
		'Name', 'Julia Set Visualizer by Frederick Ding', ...
		'NumberTitle', 'off', 'Color', 'White', ...
		'PaperPositionMode', 'auto');

	% model with default values
	q = JuliaQuadratic(position(3),position(4));
	colormap(jet(128));

	% begin drawing
	handles(2) = axes('Parent', handles(1), 'XLim', q.Boundaries(1:2), ...
		'YLim', q.Boundaries(3:4), ...
		'DataAspectRatio', [1 1 1], 'DataAspectRatioMode', 'manual', ...
		'XTick', [], 'YTick', []);
	set(handles(1), 'CurrentAxes', handles(2)); % draw fractal into handles(2)
	handles = draw(q,handles);
	
	disp(['Rendering using: ' get(handles(1), 'Renderer')]);
	
	% make UI controls
	% real part
	handles(4) = uicontrol('Style', 'slider', 'Min', -1, 'Max', 1, ...
					'Value', real(q.C));
	handles(5) = uicontrol('Style', 'text', 'String', ...
					sprintf('real part: %.3f', real(q.C)));
	% imaginary part
	handles(6) = uicontrol('Style', 'slider', 'Min', -1, 'Max', 1, ...
					'Value', imag(q.C));
	handles(7) = uicontrol('Style', 'text', 'String', ...
					sprintf('imag part: %.3f i', imag(q.C)));
	% save & load
	handles(8) = uicontrol('Style', 'pushbutton', 'String', 'Save');
	handles(9) = uicontrol('Style', 'pushbutton', 'String', 'Load');
	% colormap
	handles(10) = uicontrol('Style', 'popupmenu', 'String', ...
		'jet|hot|summer');
	handles(11) = uicontrol('Style', 'text', 'String', 'colours');
	% fidelity
	handles(12) = uicontrol('Style', 'checkbox', 'Value', 0, 'ToolTip', ...
		'Use this to increase iterations; high time expense');
	handles(13) = uicontrol('Style', 'text', 'String', 'higher res');

	% lay out UI controls
	set(handles(4:end), 'Units', 'normalized', 'BackgroundColor', ...
		get(handles(1),'Color'));
	set(handles(4), 'Position', [0.05 0.02 0.2 0.03]);
	set(handles(5), 'Position', [0.05 0.05 0.2 0.05]);
	set(handles(6), 'Position', [0.25 0.02 0.2 0.03]);
	set(handles(7), 'Position', [0.25 0.05 0.2 0.05]);
	set(handles(8), 'Position', [0.75 0.02 0.1 0.05]);
	set(handles(9), 'Position', [0.85 0.02 0.1 0.05]);
	set(handles(10), 'Position', [0.5 0.01 0.1 0.04]);
	set(handles(11), 'Position', [0.5 0.05 0.1 0.05]);
	set(handles(12), 'Position', [0.65 0.02 0.05 0.03]);
	set(handles(13), 'Position', [0.625 0.05 0.1 0.05]);
	
	attachCallbacks(q,handles);
end
function attachCallbacks(q,handles)
%%ATTACHCALLBACKS Installs the hooks for all relevant callbacks
	% The reason this is in a function is that when new figures are loaded,
	% q changes to become a reference to a new object, and the existing q
	% remains pointing to the old object.
	set(zoom,'ActionPostCallback',{@zoomPanCallback,q,handles});
	set(pan,'ActionPostCallback',{@zoomPanCallback,q,handles});

	set(handles(1), 'ResizeFcn', {@resizeCallback,q,handles});
	set(handles(4), 'Callback', {@constantChanged,q,handles}); % real
	set(handles(6), 'Callback', {@constantChanged,q,handles}); % imaginary
	set(handles(8), 'Callback', {@saveClicked,q,handles});
	set(handles(9), 'Callback', {@loadClicked,handles});
	set(handles(10), 'Callback', {@colormapChanged,q,handles});
	set(handles(12), 'Callback', {@fidelityChanged,q,handles});
	
	% even though the toolbar looks like standard options, they aren't! 
	% override callback functions to make them go to my custom save/load
	set(findall(handles(1), 'tag', 'Standard.FileOpen'), ...
		'ClickedCallback', {@loadClicked, handles});
	set(findall(handles(1), 'tag', 'Standard.SaveFigure'), ...
		'ClickedCallback', {@saveClicked,q,handles});
end
function handles = draw(q,handles)
	handles(3) = image('XData', q.Boundaries(1:2), ...
		'YData', q.Boundaries(3:4), ...
		'CData', imfilter(q.Colours, fspecial('gaussian', [3 3], 0.6)));
	% if Image Toolbox isn't installed, this needs to be
	%	'CData', q.Colours);
	% it would be too inefficient to search for the existence of an
	% imfilter function with exist('imfilter') on every redraw
	shading flat;
	title(sprintf('Julia set with f_c(z) = z^2 + c   c = %.3f + %.3fi', ...
		real(q.C), imag(q.C)));

	% disable certain buttons / features
	disableList = [ findall(handles(1), 'tag', 'Exploration.Rotate') ...
		findall(handles(1), 'tag', 'figMenuRotate3D') ...
		findall(handles(1), 'tag', 'Annotation.InsertLegend') ...
		findall(handles(1), 'tag', 'Annotation.InsertColorbar') ...
		findall(handles(1), 'tag', 'Plottools.PlottoolsOn') ...
		findall(handles(1), 'tag', 'Plottools.PlottoolsOff') ...
		findall(handles(1), 'tag', 'Standard.EditPlot') ...
		findall(handles(1), 'tag', 'Standard.NewFigure') ...
		findall(handles(1), 'tag', 'DataManager.Linking') ...
		];
	set(disableList, 'Enable', 'off', 'Visible', 'off', 'Separator', 'off');
end

function redraw(q,handles)
	handles(3) = image('XData', q.Boundaries(1:2), ...
		'YData', q.Boundaries(3:4), ...
		'CData', imfilter(q.Colours, fspecial('gaussian', [3 3], 0.6)));
	% if Image Toolbox isn't installed, this needs to be
	%	'CData', q.Colours);
	% it would be too inefficient to search for the existence of an
	% imfilter function with exist('imfilter') on every redraw
		
	title(sprintf('Julia set with f_c(z) = z^2 + c   c = %.3f + %.3fi', ...
		real(q.C), imag(q.C)));
end

function zoomPanCallback(~,~,q,handles)
	% Callback function for zoom and pan
	q.setBoundaries([get(handles(2),'XLim') get(handles(2),'YLim')]);
	% disp(num2str(q.Boundaries));
	redraw(q,handles);
end

function resizeCallback(~,~,q,handles)
	% Callback function for figure window resize
	position = get(handles(1), 'Position');
	q.resize(position(3:4));
	redraw(q,handles);
end

function constantChanged(~,~,q,handles)
%CONSTANTCHANGED Updates and recalculates for a changed constant value
	q.setConstant(complex(get(handles(4),'Value'),get(handles(6),'Value')));
	set(handles(5),'String', sprintf('real part: %.3f', real(q.C)));
	set(handles(7),'String', sprintf('imag part: %.3f i', imag(q.C)));
	redraw(q,handles);
end

function colormapChanged(object,~,q,handles)
%COLORMAPCHANGED Updates the colormap based on index of the dropdown
	if(get(handles(12), 'Value')) % check whether we are in high res
		% need 256 colours
		depth = 256;
	else
		depth = 128;
	end
	switch(get(object, 'Value'))
		case 1
			colormap(jet(depth));
		case 2
			colormap(hot(depth));
		case 3
			colormap(summer(depth));
		otherwise
			colormap(jet(depth));
	end
	redraw(q,handles);
end

function fidelityChanged(object,~,q,handles)
%FIDELITYCHANGED Updates the number of iterations and escape radius
	% the term fidelity might not be most appropriate; resolution is really
	% what this changes
	if(get(object, 'Value'))
		% checked: turn on
		q.setFidelity(1);
	else
		q.setFidelity(0);
	end
	colormapChanged(handles(10),0,q,handles); % also update the # of 
			%colours in the colormap and let that handle the redraw
end

function saveClicked(~,~,q,handles)
	saveFractalData(q,handles);
end

function loadClicked(~,~,handles)
	[q,colours] = loadFractalData();
	if(isequal(q,0))
		return
	end
	set(handles(2), 'XLim', q.Boundaries(1:2), 'YLim', q.Boundaries(3:4));
	handles = draw(q,handles);
	
	% also update UI elements
	set(handles(4), 'Value', real(q.C));
	set(handles(5), 'String', sprintf('real part: %.3f', real(q.C)));
	% imaginary part
	set(handles(6), 'Value', imag(q.C));
	set(handles(7), 'String', sprintf('imag part: %.3f i', imag(q.C)));
	% check whether it is a highres fractal
	if(q.Iterations <= 128)
		% nope
		set(handles(12), 'Value', 0);
	else
		set(handles(12), 'Value', 1);
	end
	% colormap
	set(handles(10), 'Value', colours);
	colormapChanged(handles(10),0,q,handles);
	
	attachCallbacks(q,handles);
end

function success = saveFractalData(q,handles)
%SAVEFRACTALDATA Stores the important parameters for the fractal
	% Creates a copy of the active fractal without its X,Y,Z, colour data,
	% and saves it to a .mat or image file of the user's choice.
	
	% need a complete replacement for MATLAB's figure save, so I took the
	% time to put common formats in a suitable order with descriptions.
	% obviously, *.mat is preferred (see documentation)
	[filename,path,filterindex] = uiputfile(...
		{'*.mat', 'Visualizer MATLAB files (*.mat)'; ... 
		 '*.png', 'PNG image files (*.png)'; ...
		 '*.eps', 'Encapsulated PostScript files (*.eps)'; ...
		 '*.pdf', 'Portable Document Format files (*.pdf)'; ...
		 '*.bmp', 'Bitmap image files (*.bmp)'; ...
		 '*.emf', 'Enhanced metafile files (*.emf)'; ...
		 '*.tif;*.tiff', 'Tagged Image Format files (*.tif,*.tiff)'; ...
		 '*.jpg;*.jpeg', 'Lossy JPEG image files (*.jpg,*.jpeg)' }, ...
		'Save fractal as...');
	
	if(isequal(filename,0) || isequal(path,0))
		success = 0;
	else
		fractal = q.copy();
		destination = fullfile(path, filename);
		if (filterindex == 1)
			fractal_colours = get(handles(10), 'Value');
			save(destination, 'fractal', 'fractal_colours');
		else
			% use MATLAB save figure feature
			saveas(handles(1), destination);
		end
		
		if exist(destination, 'file')
			msgbox('Saved successfully!', 'Save');
			success = 1;
		else
			msgbox('Save failed!','Save','error');
			success = 0;
		end
	end
end

function [open,fractal_colours] = loadFractalData()
%LOADFRACTALDATA Loads the parameters for a fractal
	% Loads the data for a fractal from a .mat file (assumes its
	% correctness in format) and regenerates the full JuliaQuadratic object
	% with X,Y,Z, and colour data.
	fractal_colours = 1; % default; will get overwritten if 
						  % it's in the fractal file
	[filename,path,~] = uigetfile('*.mat','Open fractal...');
	if(isequal(filename,0) || isequal(path,0))
		open = 0;
	else
		load(fullfile(path, filename), '-regexp', 'fractal*');
		if(exist('fractal','var'))
			open = JuliaQuadratic(fractal);
		else
			open = 0;
		end
	end
end