# Julia Set Visualizer
by [Frederick Ding](http://www.frederickding.com/), E'15 for EAS 105, Spring 2013

## Background

The [Julia set](http://en.wikipedia.org/wiki/Julia_set) is an escape-time fractal in complex space. The quadratic polynomial family is defined by the equation _f_(_z_) = _z_^2 + _c_ (where _c_ is a complex parameter).

## Usage

Extract all of the files of the project (but only the 2 .m files are necessary to run), then run the command `JuliaMain` from that path within MATLAB, or otherwise execute the `JuliaMain.m` function file.

### How to...

  1. **Zoom & pan:** Zoom and pan tools have been exposed through the toolbar. You can pan within the initial boundaries, which are fixed to encompass the core fractal.
  2. **Change the complex parameter:** (_c_ = _a_ + _b_ * _i_, within _a,b_ &isin; [-1,1]) Use the sliders: the left-most slider indicates the real part (_a_), and the second slider indicates the imaginary part (_b_).
  3. **Change the colour scheme:** Use the dropdown menu to select from a fixed set of existing colormaps.
  4. **Save & load:** Use either the buttons in the toolbar or the controls in the bottom-right corner. You can save as .mat data files or as static images (such as PNG and JPEG). When opening a .mat file saved with the program, it loads an interactive fractal which restores the initial parameters and perspective.

## Functionality

This program visualizes the quadratic polynomial family, with colour representing iterations until a location of _(x,y)_ gains a _z_ coordinate that exceeds a predefined escape radius. Through code, it enables the operations described above under **How to**.

### About zooming, panning, and resizing

MATLAB, by default, magnifies pixels. In this program, I have implemented functions that recalculate the fractal at the appropriate resolution and boundaries when zooming, panning, and resizing.

### About saving & loading

**Default behaviour is bad:** Figure files (.fig) are not portable enough. MATLAB includes tens of thousands of matrix elements from active variables representing position and colour when exporting them. Moreover, when such a .fig is loaded directly in a different workspace, the figure loses interactivity because callback references are no longer present; zoom reverts to pixelated magnification, for instance.

**.mat files:** The approach I have taken with .mat files harnesses native MATLAB capabilities with `save()` and `load()`. A temporary copy of the active `JuliaQuadratic` object is prepared for serialization by removing all of the X, Y, Z, and Colour data; the instance shrinks from megabytes to under a kilobyte. When such a file is loaded by the application, the class recalculates each pixel. *This format is ideal for storage and sharing, between people who have the program.*

**UI customizations:** The **Save** or **Load** `uicontrol` buttons in the bottom-right corner and those in the figure toolbar have been customized to use the same callbacks. Common formats are exposed in the save dialog, ordered so that .mat files are default. When a static image format is chosen, `saveas()` is used to produce a snapshot of the figure.

### About other MATLAB techniques

**Vectorization:** MATLAB's speed in vector-wise and matrix-wise operation was the reason that some calculations, such as the core quadratic polynomial equation (see `JuliaQuadratic.recalculate()`), were done with indexing and vector operations, rather than nested loops, as much as possible.

**Images and colormaps:** Viewers such as these inevitably make use of MATLAB images to showcase a 2D plane that conveys more information by way of colour. By default, the `jet` colormap is utilized, as the `JuliaQuadratic.Colours` matrix contains indices over the length of the colormap. A light amount of Gaussian blur through the Image Toolkit is used to soften the noise.

**Handles:** Handles are used extensively in this program, both for figures and axes, and for object-oriented programming (see below). They are used to `set()` properties that would otherwise be difficult to do in a constructor. For instance, figure toolbar items are modified in this manner.

**Object-oriented programming (OOP):** While not a component of the EAS 105 course, certain OOP paradigms fit well with this program's purpose. Although Julia sets can be calculated procedurally, the use of OOP assists with passing data between parts of this program by sequestering data with rigid controls on access/modification, and assists with save & load customizations. The style with which objects and callbacks are used in this program are reminiscent of Java.

**Unimaginable amounts of debugging.**

----------

<small>**Why not a Mandelbrot viewer?** They're a classic, but overdone. I wanted to do something different with more options that can be changed in a GUI (varying _c_)!</small>

<small>**What does higher res do?** It switches from 128 iterations (hence, 128-colour map) to 256 iterations (and 256-colour map), at the expense of extra computational time. This trade-off is the reason that it is off by default.</small>

<small>**Why add blur?** MATLAB doesn't do any antialiasing by default. In a fractal such as this, one single coordinate could have a radically different colour from one single neighbouring pixel because they are discrete samples of an infinite set, which leads to [noisy and unsavoury images](http://d.fjd.me/H9l3). To compensate, a light Gaussian filter was added.</small>

<small>**Where can I see a demo?** A screen recording of a pre-release version: [http://www.youtube.com/watch?v=dV-pLp41cEE&hd=1](http://www.youtube.com/watch?v=dV-pLp41cEE&hd=1).</small>

<!-- $Id$ -->