% Frederick Ding
% Version (Git hash): $Id$

classdef JuliaQuadratic < matlab.mixin.Copyable & handle
	%JULIAQUADRATIC Special case of quadratic polynomials
	%   Calculates and stores a representation of a member of the quadratic
	%   polynomial family, where $$f_c(z) = z^2 + c$$.
	
	properties (SetAccess = private)
		Height = 600;
		Width = 600;
		
		Iterations = 128;
		C = -0.8 + 0.156*1i; % the complex parameter
		Escape = 2;
		Boundaries = [-2 2 -2 2];

		X
		Y
		Z
		Colours % colours
	end
	
	methods
		% this constructor is pseudo-overloaded; uses:
		% - JuliaQuadratic() = uses defaults
		% - JuliaQuadratic(width, height) = self-explanatory
		% - JuliaQuadratic(copy) = import non-XYZC data from this struct
		function q = JuliaQuadratic(arg1,arg2)
			if (nargin == 2)
				q.Width = arg1;
				q.Height = arg2;
				% adjust boundaries to accommodate shape of height to width
				new_ratio = arg2/arg1;
				if(new_ratio > 1) % thinner
					% resize using the existing height
					q.Boundaries = [-q.Boundaries(4) / new_ratio ...
						q.Boundaries(4) / new_ratio ...
						q.Boundaries(3:4)];
				else
					% resize using the existing width
					q.Boundaries = [q.Boundaries(1:2) ...
						-q.Boundaries(2) * new_ratio ...
						q.Boundaries(2) * new_ratio];
				end
			elseif (nargin == 1 && isequal(class(arg1), 'JuliaQuadratic'))
				% needs to be rejuvenated
				q = arg1;
			end
			q.resetXYZ;
			q.recalculate;
		end
		function resetXYZ(q) % reinitializes XYZC
			[q.X,q.Y] = meshgrid(...
				linspace(q.Boundaries(1), q.Boundaries(2), q.Height), ...
				linspace(q.Boundaries(3), q.Boundaries(4), q.Width));
			q.Z = q.X + q.Y*1i; % complex plane
			q.Colours = ones(size(q.Z));
		end
		function recalculate(q) % used for initial calcs 
								% & after parameter changes
			z_indexer = 1:numel(q.Z);
			for k=1:q.Iterations;
				% the CORE QUADRATIC POLYNOMIAL
				q.Z(z_indexer) = q.Z(z_indexer).^2 + q.C;
				% only recalculate pixels that haven't already exceeded the
				% escape distance; we use escape distance for colouring
				z_indexer = z_indexer( abs(q.Z(z_indexer)) < q.Escape );
				if(k ~= q.Iterations)
					% change colours for any pixels that are still being
					% considered for iteration
					q.Colours(z_indexer) = mod(k-1, q.Iterations - 1) + 2;
				end
			end
		end
		function setConstant(obj, new_c)
			if (isreal(new_c))
				error('Must be a complex parameter')
			end
			obj.C = new_c;
			obj.resetXYZ();
			obj.recalculate();
		end
		function setBoundaries(obj, new_lim)
			if ~(isequal(size(new_lim), [1 4]))
				error('Must be a 4 element row vector')
			end
			obj.Boundaries = new_lim;
			% disp('boundaries set');
			obj.resetXYZ();
			obj.recalculate();
		end
		function setFidelity(obj, boolean_switch)
			% by default, the class uses only 128 iterations, which reduces
			% the depth of colour and detail, so some areas seem to be 
			% disappearing into the abyss (red in jet colormap, white in 
			% hot colormap). high fidelity will increase iterations,
			% at the expense of rendering time
			% I think high res doesn't look that much better
			if(boolean_switch)
				obj.Iterations = 256;
				disp('High fidelity on');
			else
				% revert to lower fidelity
				obj.Iterations = 128;
				disp('High fidelity off');
			end
			obj.resetXYZ();
			obj.recalculate();
		end
		function resize(q,new_size)
			if ~(isequal(size(new_size), [1 2]))
				error('Must be a 2 element row vector indicating [width height]')
			elseif (new_size(1) <= 0 || new_size(2) <= 0)
				error('Must be non-zero and positive')
			end
			ratio = q.Height/q.Width;
			new_ratio = new_size(2) / new_size(1);
			fprintf('Old ratio: %f. New ratio: %f\n', ratio, new_ratio);
			% determine if it is getting fatter or thinner
			if(new_ratio > ratio) % thinner
				% resize using the existing height
				q.Boundaries = [-q.Boundaries(4) / new_ratio ...
					q.Boundaries(4) / new_ratio ...
					q.Boundaries(3:4)];
			else
				% resize using the existing width
				q.Boundaries = [q.Boundaries(1:2) ...
					-q.Boundaries(2) * new_ratio ... 
					q.Boundaries(2) * new_ratio];
			end
			disp(['New bounds: ' num2str(q.Boundaries)]);
			q.Width = new_size(1);
			q.Height = new_size(2);
			q.resetXYZ();
			q.recalculate();
		end
		
		function deleteXYZ(q) % used only for serialization!
			q.X = [];
			q.Y = [];
			q.Z = [];
			q.Colours = [];
		end
	end
	methods (Access = protected)
		function cp = copyElement(q)
			% the point of this method is to help with serialization, by
			% preserving only the non-calculated parameters.
			cp = copyElement@matlab.mixin.Copyable(q);
			cp.deleteXYZ(); % this is so important!!!
		end
	end
	
end

